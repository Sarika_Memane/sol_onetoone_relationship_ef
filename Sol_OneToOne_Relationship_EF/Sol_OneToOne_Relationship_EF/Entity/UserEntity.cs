﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_OneToOne_Relationship_EF.Entity
{
   public class UserEntity
    {
        public decimal? UserId { get; set; }

        public String FirstName { get; set; }

        public String LastName { get; set; }

        public UserLoginEntity UserLogin { get; set; }

        public UserCommunicationEntity UserCommunication { get; set; }
    }
}
