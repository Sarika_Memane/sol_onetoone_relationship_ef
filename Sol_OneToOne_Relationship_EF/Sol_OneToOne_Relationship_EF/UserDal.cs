﻿using Sol_OneToOne_Relationship_EF.EF;
using Sol_OneToOne_Relationship_EF.Entity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_OneToOne_Relationship_EF
{
   public class UserDal
    {
        #region Declaration
        private TestDBEntities db = null;
        #endregion

        #region Constructor
         public UserDal()
        {
            db = new TestDBEntities();
        }
        #endregion

        #region Public Methods
        public async Task<IEnumerable<UserEntity>> GetUserData()
        {
            return await Task.Run(()=>
            {
                var getQuery =
                   db
                   ?.tblUsers
                   ?.AsEnumerable()
                   ?.Select(
                       (letblUserObj) => new UserEntity()
                       {
                           UserId = letblUserObj?.UserId ?? 0,
                           FirstName = letblUserObj?.FirstName ?? null,
                           LastName = letblUserObj?.LastName ?? null,
                           UserLogin = new UserLoginEntity()
                           {
                               UserName = letblUserObj?.tblUserLogin?.UserName ?? null,
                               Password = letblUserObj?.tblUserLogin?.Password ?? null
                           },
                           UserCommunication = new UserCommunicationEntity()
                           {
                               EmailId = letblUserObj?.tblUsersCommunication?.EmailId ?? null,
                               MobileNo = letblUserObj?.tblUsersCommunication?.MobileNo ?? null
                           }
                       }

                       )
                       ?.ToList();

                return getQuery;
            });
        }
   
        }
        #endregion
    }
