﻿using Sol_OneToOne_Relationship_EF.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_OneToOne_Relationship_EF
{
    class Program
    {
        static void Main(string[] args)
        {
            Task.Run(async () =>
            {
                IEnumerable<UserEntity> userEntityObj = await new UserDal().GetUserData();

            }).Wait();
        }
    }
}
